<?php

/**
 * @file
 * Contains \Drupal\pdf_generator\PdfGeneratorPluginManager.
 */

namespace Drupal\pdf_api;

use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages PDF generator plugins.
 */
class PdfGeneratorPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces) {
    parent::__construct('Plugin/PdfGenerator', $namespaces, 'Drupal\pdf_api\Annotation\PdfGenerator');
  }
}
